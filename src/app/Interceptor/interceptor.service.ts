import { Injectable } from '@angular/core';
import {
  HttpEventType,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { tap } from 'rxjs';
import { IUserValidation } from '../app.types';
@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  getToken: string = localStorage.getItem('token') || '';
  baseURL:string = 'http://localhost:3000';
  constructor() {}
  intercept(req: HttpRequest<IUserValidation>, next: HttpHandler) {
    let tokenReq = req.clone({
      url: this.baseURL + req.url,
      setHeaders: {
        Authorization: `${localStorage.getItem('token') || ''}`,
      },
    });

    return next.handle(tokenReq).pipe(
      tap((event) => {
        if (event.type === HttpEventType.Response) {
       
        }
      })
    );
  }
}
