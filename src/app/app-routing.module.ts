import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './custom-modules/auth/services/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./custom-modules/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./custom-modules/admin/admin.module').then((m) => m.AdminModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'doctor',
    loadChildren: () =>
      import('./custom-modules/doctor/doctor.module').then(
        (m) => m.DoctorModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'nurse',
    loadChildren: () =>
      import('./custom-modules/nurse/nurse.module').then((m) => m.NurseModule),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
