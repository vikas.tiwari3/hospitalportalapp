import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './custom-modules/auth/auth.module';
import { SharedModule } from './custom-modules/shared/shared.module';
import { AdminModule } from './custom-modules/admin/admin.module';
import { DoctorModule } from './custom-modules/doctor/doctor.module';
import { NurseModule } from './custom-modules/nurse/nurse.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from './custom-modules/auth/services/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './Interceptor/interceptor.service';
import { AuthGuard } from './custom-modules/auth/services/auth.guard';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AuthModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [AuthService, {
    provide:HTTP_INTERCEPTORS,
    useClass: InterceptorService,
    multi:true
  },AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
