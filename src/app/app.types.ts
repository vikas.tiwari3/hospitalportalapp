export interface IUserValidation{
    email:string,
    password:string
}

export interface IUserValidationResult{
    data:IUserValidationResultData
}

export interface IUserValidationResultData{
    token:string,
    userId:string,
    role:string,
    name:string,
    tokenExpiry:number
}

export interface IRequestResponse{
    data:IRequestResponseData
}
export interface IRequestResponseData{
    _id:string,
    name:string,
    email:string,
    password:string,
    role:string,
    assignedDoctor:IAssignedDoctor,
    speciality:string,
    nurses:IUserData,
    resetToken:string,
    resetTokenExpiry:string,
    createdAt:string,
    updatedAt:string,
    __v:string
}

export interface IAssignedDoctor {
    name:string;
}

export interface IUserData{
name:string,
_id:string
}

export interface INurseList{
    nursename: string,
    doctorname: string
}

export interface IRequestList {
    request:string
}

export interface IChangeRequestResult {
    data: IChangeRequestResultData
}
export interface IChangeRequestResultData{
    _id:string,
    from:IRequestFrom,
    for:[],
    replacement:[],
    reason:string,
    status:[IStatus],
    createdAt:string,
    updatedAt: string,
}
export interface IStatus {
    _id:string,
    title:string,
    type:string
}

export interface IRequestFrom{
    id:string,
    name:string,
    email:string,
    role:string,
}

export interface IRegisterDoctor{
    name:string,
    email:string,
    speciality:string,
    role:string,
    nurses:[]
}

export interface IDeleteUser{
    _id:string
}

export interface IAddNurse{
    name:string,
    email:string,
    role:string,
    assignedDoctor:string
}

export interface IChangeNurse{
    for:string,
    replacement:string,
    reason:string
}
export interface IMessage{
    text:string,
    to:string
}

export interface IMessageRequestResult {
    data: IMessageRequestResultData
}

export interface IMessageRequestResultData{
    _id:string,
    from:[],
    to:string,
    text:string,
}

export interface IReminder{
    data:IReminderResult
}

export interface IReminderResult{
    request:string,
    _id:string,
    createdAt:string,
    updatedAt:string
}