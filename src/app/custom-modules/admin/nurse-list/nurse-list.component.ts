import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { INurseList, IRequestResponseData, IUserData } from 'src/app/app.types';
import { ViewChild } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

@Component({
  selector: 'app-nurse-list',
  templateUrl: './nurse-list.component.html',
  styleUrls: ['./nurse-list.component.scss']
})

export class NurseListComponent implements OnInit {

displayAddPopup:boolean = false;
displayEditPopup:boolean = false;
value:string = '';
userId:string = '';
form!:FormGroup;
editForm!:FormGroup;
resultantData:IRequestResponseData[] = [];
doctorList:IRequestResponseData[] = [];
nurseDataList:IRequestResponseData[] = [];
  displayedColumns = [
    'nursename',
    'doctorname',
    'edit',
    'delete'
  ]
  dataSource!: MatTableDataSource<INurseList>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  constructor(private adminService:AdminService, public fb:FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name:new FormControl(''),
      email:new FormControl('',[Validators.required, Validators.email]),
      role:new FormControl(''),
      assignedDoctor:new FormControl('')
    })
    this.editForm = this.fb.group({
      name: new FormControl('')
    })
    this.getNurseList();
  }

  ngAfterViewInit() {
    
  }
  getNurseList(){
      this.adminService.getUserList('user').subscribe({
        next:(res)=>{
          const data = Object(res).data.filter((ele: { role: string; }) => ele.role === '62d0f28c83b297e5b2036451');
          this.dataSource = new MatTableDataSource(Object(data));
          this.dataSource.paginator = this.paginator;
          this.doctorList = Object(res).data.filter((ele: { role: string; }) => ele.role === '62d0f28c83b297e5b2036450');
        }
    })
  }

  addNurse(form:FormGroup){
    
      form.get('role')?.setValue('62d0f28c83b297e5b2036451');
      this.adminService.addUser(form.value).subscribe((res)=>{
      this.getNurseList();
      alert('User Registered Successfully');
      this.displayAddPopup = !this.displayAddPopup;
    })
  }

  deleteNurse(element:IUserData){
      this.adminService.deleteData(`user/${element._id}`).subscribe((res)=>{
      this.getNurseList();
  })
}

  editNurse(nurseData:FormGroup){
    this.adminService.editUser(nurseData.value,this.userId).subscribe((res)=>{
   })
}

addNursePopup(){
  this.displayAddPopup = !this.displayAddPopup;
}
editNursePopup(element:IUserData){
  this.displayEditPopup = !this.displayEditPopup;
  this.userId = element._id;
}
}