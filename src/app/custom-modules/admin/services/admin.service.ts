import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IAddNurse, IUserData, IRequestResponse, IRequestResponseData, IRegisterDoctor, IChangeRequestResult, IRequestList } from 'src/app/app.types';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private httpClient:HttpClient) { }
  doctorList = [{name:'doctor1'}, {name: 'doctor2'}, {name:'doctor3'}, {name: 'doctor4'},{name: 'doctor5'}];

  getUserList(path:string):Observable<IRequestResponse>{
return this.httpClient.get(`/${path}`) as Observable<IRequestResponse>;
  }

  addUser(userData:IRegisterDoctor): Observable<IRegisterDoctor>{
   return this.httpClient.post(`/user/register`, userData) as Observable<IRegisterDoctor>;
  }

  deleteData(path:string):Observable<any>{
    return this.httpClient.delete(`/${path}`);
  }

  editUser(userData:IRequestResponseData, userId:string):Observable<IUserData>{
    return this.httpClient.patch(`/user/${userId}`, userData) as Observable<IUserData>;
  }

  changeRequestStatus(id:string,status:string):Observable<IRequestResponseData>{
    const data = { status: status };
    return this.httpClient.patch(`/change-request/${id}`,data) as Observable<any>;
  }
}
