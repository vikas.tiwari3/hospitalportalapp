import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NurseListComponent } from './nurse-list/nurse-list.component';
import { RequestsComponent } from './requests/requests.component';
const routes: Routes = [
    {path: '', component: DoctorListComponent},
    {path: 'doctors', component: DoctorListComponent},
    {path: 'nurses', component: NurseListComponent},
    {path: 'requests', component: RequestsComponent},
    {path: 'notifications', component: NotificationsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }