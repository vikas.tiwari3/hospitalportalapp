import { Component, OnInit } from '@angular/core';
import { IDeleteUser, IUserData, IRequestResponseData } from 'src/app/app.types';
import { AdminService } from '../services/admin.service';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.scss']
})

export class DoctorListComponent implements OnInit {
  form!:FormGroup;
  editForm!:FormGroup;
  value:string[] = [];
  displayAddPopup:boolean = false;
  displayEditPopup:boolean = false;
  userId:string = '';
  userName:string = '';
  resultantData:IRequestResponseData[] = [];
  doctorDataList:IRequestResponseData[] = [];
  nurseList:IUserData[] = [];
  displayedColumns = [
    'name',
    'speciality',
    'nurses',
    'edit',
    'delete'
  ] 
  dataSource!: MatTableDataSource<IUserData>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('table') myTable!: MatTable<any>;
  constructor(private adminService:AdminService, public fb: FormBuilder) { }

  ngOnInit(): void 
  {
      this.form = this.fb.group(
        {
      name: new FormControl(''),
      email:new FormControl('', [Validators.required, Validators.email]),
      speciality: new FormControl(''),
      role: new FormControl(''),
      nurses: new FormControl('')
      })
    this.editForm = this.fb.group(
      {
      name:new FormControl('')
      })
    this.getDoctorList();

  }

ngAfterViewInit() 
{
  this.getDoctorList();

}

getDoctorList()
{
  this.adminService.getUserList('user').subscribe(
    {
      next: (response) => 
      {
      const data = Object(response).data.filter((ele: { role: string; }) => ele.role === '62d0f28c83b297e5b2036450');
      this.dataSource = new MatTableDataSource(Object(data));
      this.dataSource.paginator = this.paginator;
      this.nurseList = Object(response).data.filter((ele: { role: string; })=>ele.role === '62d0f28c83b297e5b2036451');
     }
  })
}

addDoctor()
{
  this.displayAddPopup = !this.displayAddPopup;
}

editDoctorPopup(element:any)
{
  this.displayEditPopup = !this.displayEditPopup;
  this.userId = element._id;
  this.userName = element.name;
  console.log(this.userId)
}

saveDoctor(form:FormGroup)
{
  form.get('role')?.setValue('62d0f28c83b297e5b2036450')
  this.adminService.addUser(form.value).subscribe((res)=>
  {
    alert(Object(res).data.message);
    this.displayAddPopup = !this.displayAddPopup;
    this.getDoctorList();
  })
}

  editDoctor(userData:FormGroup)
  {
   console.log(userData.value)
    this.adminService.editUser(userData.value,this.userId).subscribe((res)=>
    {
    this.displayEditPopup = !this.displayEditPopup;
      this.getDoctorList();
    })
  }

  deleteDoctor(element:IDeleteUser)
  {
    this.adminService.deleteData(`user/${element._id}`).subscribe((res)=>
    {
      this.getDoctorList();
    })  
  }
}
