import { Component, OnInit } from '@angular/core';
import { IReminder, IReminderResult, IChangeRequestResultData } from 'src/app/app.types';
import { AdminService } from '../services/admin.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notificationList:IReminder[] = [];

 displayedColumns = [
  'notification',
  'createdAt',
  'delete'
 ]

 @ViewChild(MatPaginator) paginator!:MatPaginator;

 dataSource = new MatTableDataSource<IReminderResult>;


  constructor(private adminService:AdminService) { }

  ngOnInit(): void {
    this.getNotificationList();
    
  }
  ngAfterViewInit(){

  }

  getNotificationList(){
    this.adminService.getUserList('reminder?deleted=false').subscribe({
      next:(res)=>{
            this.notificationList = Object(res).data;
            this.dataSource = new MatTableDataSource(Object(this.notificationList));
            this.dataSource.paginator = this.paginator;
      }
    })
  }

  deleteNotification(element:IChangeRequestResultData){
      this.adminService.deleteData(`reminder/${element._id}`).subscribe((res)=>{
      this.getNotificationList();
    })
  }

}


