import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { IUserData, IRequestList, IChangeRequestResultData, IStatus } from 'src/app/app.types';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  resultantData:IUserData[] = [];
  requestDataList:IChangeRequestResultData[] = [];
  pendingRequests:IStatus[]=[];
  displayedColumns = [
    'name',
    'reason',
    'approve',
    'reject',
  ]

  dataSource!: MatTableDataSource<IChangeRequestResultData>;
  @ViewChild(MatPaginator) paginator!:MatPaginator;

  constructor(private adminService:AdminService) { }

  ngOnInit(): void 
  {
    this.getRequest();
  }

ngAfterViewInit() 
{

}

  getRequest()
  {
      this.adminService.getUserList('change-request').subscribe({
        next: (res)=>{
          const data = Object(res).data.filter((ele: { status: { title: string; }[]; })=> ele.status[0].title === 'pending');
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.paginator;
        }
      }
      )}

  approveRequest(requestData:IUserData)
  {
    this.adminService.changeRequestStatus(requestData._id,'62d0f28c83b297e5b2036456').subscribe((res)=>{})
  }

  rejectRequest(requestData:IUserData)
  {
    this.adminService.changeRequestStatus(requestData._id,'62d0f28c83b297e5b2036455').subscribe((res)=>{})
  }

}
