import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IChangeNurse, IUserData, IRequestResponse, IRequestResponseData, IMessage, IMessageRequestResult, IReminder, IChangeRequestResult } from 'src/app/app.types';

@Injectable({
  providedIn: 'root'
})
export class NurseService {

  constructor(public httpClient:HttpClient) { }

  getData(path:string):Observable<IRequestResponse>{
    return this.httpClient.get(`/${path}`) as Observable<IRequestResponse>;
  }

  sendMessageToDoctor(form:IMessage):Observable<IMessage>{
    return this.httpClient.post(`/message`, form) as Observable<IMessage>;
  }

 requestChangeDoctor(changeRequestForm:IChangeNurse):Observable<IChangeNurse>{
  return this.httpClient.post(`/change-request`,changeRequestForm) as Observable<IChangeNurse>;
 }

 remindAdmin(id:string):Observable<IReminder>{
  return this.httpClient.post(`/reminder/${id}`,id) as Observable<IReminder>;
 }
}
