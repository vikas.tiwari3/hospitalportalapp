import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseRequestsComponent } from './nurse-requests.component';

describe('NurseRequestsComponent', () => {
  let component: NurseRequestsComponent;
  let fixture: ComponentFixture<NurseRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseRequestsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NurseRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
