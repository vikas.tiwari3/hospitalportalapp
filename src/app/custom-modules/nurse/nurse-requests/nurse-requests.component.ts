import { Component, OnInit, ViewChild } from '@angular/core';
import { IRequestResponseData, IUserData } from 'src/app/app.types';
import { NurseService } from '../services/nurse.service';
import { MatTableDataSource, MatTable } from '@angular/material/table';
@Component({
  selector: 'app-nurse-requests',
  templateUrl: './nurse-requests.component.html',
  styleUrls: ['./nurse-requests.component.scss']
})
export class NurseRequestsComponent implements OnInit {
  resultantData:IUserData[] = [];
  myRequest:any= []
  requestFor:string = '';
  @ViewChild('table') myTable!: MatTable<any>;

  displayedColumns=[
    'name',
    'status',
    'reminder'
  ]
  dataSource = new MatTableDataSource<IRequestResponseData>;
  constructor(private nurseService:NurseService) { }

  ngOnInit(): void {
    this.getNurseRequests();
  }

  getNurseRequests(){
    this.nurseService.getData('status').subscribe((res)=>{
      this.dataSource = Object(res).data;
    })
  }

  sendReminder(){
    this.nurseService.remindAdmin(`${localStorage.getItem('_id')}`).subscribe((res)=>
    {
    })
  }
}
