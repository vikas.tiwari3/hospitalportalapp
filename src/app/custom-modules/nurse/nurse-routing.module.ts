import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NurseHomeComponent } from './nurse-home/nurse-home.component';
import { NurseRequestsComponent } from './nurse-requests/nurse-requests.component';

const routes: Routes = [
    {path: '', component: NurseHomeComponent},
    {path: 'nurseRequest', component: NurseRequestsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NurseRoutingModule { }