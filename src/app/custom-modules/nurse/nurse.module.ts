import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NurseHomeComponent } from './nurse-home/nurse-home.component';
import { NurseRoutingModule } from './nurse-routing.module';
import { NurseRequestsComponent } from './nurse-requests/nurse-requests.component';
import { SharedModule } from '../shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NurseHomeComponent,
    NurseRequestsComponent,
  ],
  imports: [
    CommonModule,
    NurseRoutingModule,
    SharedModule,
    MatTableModule,
    MatIconModule,
    MatSelectModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class NurseModule { }
