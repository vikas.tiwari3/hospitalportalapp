import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { IRequestResponseData, IMessage, IUserData } from 'src/app/app.types';
import { NurseService } from '../services/nurse.service';

@Component({
  selector: 'app-nurse-home',
  templateUrl: './nurse-home.component.html',
  styleUrls: ['./nurse-home.component.scss']
})
export class NurseHomeComponent implements OnInit {
  displayMessagePopup: boolean = false;
  displayChangeRequestPopup:boolean = false;
  idSendMessageTo: string = '';
  nameSendMessageTo:string='';
  value:string = '';
  doctorId:string = '';
  form!: FormGroup;
  changeRequestForm!:FormGroup;
  nurseDetails: IRequestResponseData[] = [];
  availDoctors:IRequestResponseData[] = [];
  assignedDoctor: any = [];
  name: string = '';
  displayedColumns = [
    'assignedDoctor',
    'name',
    'changeRequest',
    'message'
  ]
  @ViewChild('table') myTable!: MatTable<any>;
  dataSource!: MatTableDataSource<IRequestResponseData>;

  constructor(private nurseService: NurseService, public fb: FormBuilder) { }

  ngOnInit(): void
   {
    this.form = this.fb.group(
      {
      text: new FormControl('', [Validators.required]),
      to: new FormControl('')
      })

    this.changeRequestForm = this.fb.group({
      for: new FormControl('', [Validators.required]),
      reason: new FormControl('',[Validators.required]),
      replacement: new FormControl('', [Validators.required])
    })
    this.getNurseData();
    this.name = localStorage.getItem('name')!;
  }

  getNurseData() 
  {
      this.nurseService.getData('user').subscribe(
        {
          next:(res)=>
          {
            const data = Object(res).data.filter((ele: { _id: string; })=>ele._id === `${localStorage.getItem('_id')}`);
            this.dataSource = new MatTableDataSource(data[0].assignedDoctor);
            
          this.availDoctors = Object(res).data.filter((ele: { role: string; })=>ele.role === '62d0f28c83b297e5b2036450');
        }
         
        }
    )
  }

  openMessagePopup(element: IRequestResponseData) 
  {
    this.idSendMessageTo = element._id;
    this.nameSendMessageTo = element.name;
    this.displayMessagePopup = !this.displayMessagePopup;
  }

  changeRequestPopup(doctorId:IRequestResponseData)
  {
    this.doctorId = doctorId._id;
    this.displayChangeRequestPopup = !this.displayChangeRequestPopup;
  }

  changeDoctor(changeRequestForm:FormGroup)
  {
      changeRequestForm.get('for')?.setValue(this.doctorId);
      this.nurseService.requestChangeDoctor(changeRequestForm.value).subscribe((res)=>
      {
      this.displayChangeRequestPopup = !this.displayChangeRequestPopup;
      })
      this.getNurseData();
  }

  sendMessage(form: FormGroup) 
  {
    form.get('to')?.setValue(this.idSendMessageTo);
    this.nurseService.sendMessageToDoctor(form.value).subscribe((res) =>
     {
      console.log(res);
      this.displayMessagePopup = !this.displayMessagePopup;
    })
    this.getNurseData();
  }
}
