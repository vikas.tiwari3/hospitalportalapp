import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
form!:FormGroup;
role:string = '';
setRole:string = '';
  constructor(public fb:FormBuilder, private authService:AuthService,private router:Router) { }
  ngOnInit(): void {
    this.form = this.fb.group({
      email: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('',[Validators.required, Validators.minLength(6)])
    })
  }
  userRoles = [{role:'62d0f28c83b297e5b203644f', name:'admin'},
{role: '62d0f28c83b297e5b2036450', name:'doctor'},
{role:'62d0f28c83b297e5b2036451', name:'nurse'}];

  onSubmit(form:FormGroup)
  {
    this.authService.validateUser(form.value).subscribe((res)=>
    {
      localStorage.setItem('token', res.data.token);
      localStorage.setItem('role',res.data.role);
      localStorage.setItem('name', res.data.name);
      localStorage.setItem('_id', res.data.userId);
      this.role =  localStorage.getItem('role')!;
      
      this.userRoles.map(user=>
        {
        if(user.role === this.role)
        {
        this.setRole = user.name;
        }
        })
      this.router.navigate([`${this.setRole}`]);
    });
  }
}
