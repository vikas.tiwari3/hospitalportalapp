import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUserValidation, IUserValidationResult } from 'src/app/app.types';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
baseURL:string = 'http://localhost:3000';
// role:string = localStorage.getItem('role')!;
  constructor(private httpClient:HttpClient) { }


  validateUser(userData:IUserValidation): Observable<IUserValidationResult>{
    return this.httpClient.post(`/user/login`,userData) as  Observable<IUserValidationResult>;
  }

  validateRole(){
   return `${localStorage.getItem('role')}`;
  }

}
