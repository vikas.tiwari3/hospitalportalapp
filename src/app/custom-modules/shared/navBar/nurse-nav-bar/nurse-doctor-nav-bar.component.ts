import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-nurse-nav-bar',
  templateUrl: './nurse-doctor-nav-bar.component.html',
  styleUrls: ['./nurse-doctor-nav-bar.component.scss']
})
export class NurseDoctorNavBarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  
  nurseHome(){
    this.router.navigate(['nurse']);
  }

  nurseRequest(){
    this.router.navigate(['nurse/nurseRequest']);
    console.log('nurse');
      }
      nurseMessages(){
        this.router.navigate(['nurse/nurseMessages']);
      }

}
