import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseDoctorNavBarComponent } from './nurse-doctor-nav-bar.component';

describe('NurseDoctorNavBarComponent', () => {
  let component: NurseDoctorNavBarComponent;
  let fixture: ComponentFixture<NurseDoctorNavBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseDoctorNavBarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NurseDoctorNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
