import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-left-nav-bar',
  templateUrl: './left-nav-bar.component.html',
  styleUrls: ['./left-nav-bar.component.scss']
})
export class LeftNavBarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  displayDoctors(){
    this.router.navigate(['admin']);
  }

  displayNurses(){
    this.router.navigate(['admin/nurses']);
    console.log('nurse');
      }
      displayNotifications(){
        this.router.navigate(['admin/notifications']);
      }
    
      displayRequests(){
        this.router.navigate(['admin/requests']);
      }

}
