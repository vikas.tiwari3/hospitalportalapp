import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-doctor-nav-bar',
  templateUrl: './doctor-nav-bar.component.html',
  styleUrls: ['./doctor-nav-bar.component.scss']
})
export class DoctorNavBarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  doctorHome(){
    this.router.navigate(['doctor']);
  }

  doctorRequest(){
    this.router.navigate(['doctor/doctorRequest']);
    console.log('nurse');
      }
      doctorMessages(){
        this.router.navigate(['doctor/doctorMessages']);
      }

}
