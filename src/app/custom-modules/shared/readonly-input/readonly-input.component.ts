import { Component, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-readonly-input',
  templateUrl: './readonly-input.component.html',
  styleUrls: ['./readonly-input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: ReadonlyInputComponent,
    multi:true
  }]
})
export class ReadonlyInputComponent implements OnInit, ControlValueAccessor {
  @Input('type') public type:string = '';
  @Input('placeholder') public placeholder:string = '';
  value:string = '';
  constructor() { }
  onChange!: (value: string) => void;
  onTouched!: () => void;

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit(): void {
  }

}
