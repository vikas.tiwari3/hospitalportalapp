import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './buttons/button/button.component';
import { InputComponent } from './input/input.component';
import { FormsModule } from '@angular/forms';
import { LeftNavBarComponent } from './navBar/admin-nav-bar/left-nav-bar.component';
import { NurseDoctorNavBarComponent } from './navBar/nurse-nav-bar/nurse-doctor-nav-bar.component';
import { DoctorNavBarComponent } from './navBar/doctor-nav-bar/doctor-nav-bar.component';
import { LogoutBtnComponent } from './buttons/logout-btn/logout-btn.component';
import {MatIconModule} from '@angular/material/icon';
import { AddPopupComponent } from './popups/add-popup/add-popup.component';
import { AddBtnComponent } from './buttons/add-btn/add-btn.component';
import { ReadonlyInputComponent } from './readonly-input/readonly-input.component';

@NgModule({
  declarations: [
    ButtonComponent,
    InputComponent,
    LeftNavBarComponent,
    NurseDoctorNavBarComponent,
    DoctorNavBarComponent,
    LogoutBtnComponent,
    AddPopupComponent,
    AddBtnComponent,
    ReadonlyInputComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule
  ],
  exports: [
    ButtonComponent,
    InputComponent,
    LeftNavBarComponent,
    NurseDoctorNavBarComponent,
    DoctorNavBarComponent,
    LogoutBtnComponent,
    AddPopupComponent,
    ReadonlyInputComponent,
  ]
})
export class SharedModule { }
