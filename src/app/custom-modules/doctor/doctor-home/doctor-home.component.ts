import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormControlName, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IRequestResponse, IRequestResponseData, IUserData } from 'src/app/app.types';
import { DoctorService } from '../services/doctor.service';

@Component({
  selector: 'app-doctor-home',
  templateUrl: './doctor-home.component.html',
  styleUrls: ['./doctor-home.component.scss']
})
export class DoctorHomeComponent implements OnInit {
  displayChangeRequestPopup:boolean = false;
  form!:FormGroup;
  name:string = localStorage.getItem('name')!;
  value:string = '';
  userId:string = '';
  nursesAllocatedToMe:any = [];
  allNurses:any = [];
  resultantData:IRequestResponseData[] = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource!: MatTableDataSource<IUserData>;

  displayedColumns = [
    'name',
    'changeRequest',
  ]

  constructor(private doctorService:DoctorService, public fb:FormBuilder) { }

  ngOnInit(): void 
  {
      this.form = this.fb.group
      ({
      for:new FormControl(''),
      reason: new FormControl(''),
      replacement: new FormControl('')
      })
    this.getNurses();
  }

  getNurses()
  {
      this.doctorService.getData('user').subscribe(
        {
          next:(res)=>
          {
            const data = Object(res).data.filter((ele: { _id: string; }) => ele._id === `${localStorage.getItem('_id')}`);
            this.dataSource = new MatTableDataSource(data[0].nurses);
            this.dataSource.paginator = this.paginator;
          }
        }
    )
  }
  getSelectedNurse(element:IUserData)
  {
    this.displayChangeRequestPopup = !this.displayChangeRequestPopup
    this.userId = element._id;
  }

  changeNurse(form:FormGroup)
  {
    form.get('for')?.setValue(`${this.userId}`);
    this.doctorService.changeNurse(form.value).subscribe((res)=>
    {
      this.displayChangeRequestPopup = !this.displayChangeRequestPopup;
    })
    this.getNurses();
  }

  getDoctorRequests()
  {
    this.doctorService.getData('status').subscribe((res)=>{
      this.dataSource = Object(res).data;
    })
  }
  
}
