import { Component, OnInit, ViewChild } from '@angular/core';
import { getMatIconFailedToSanitizeLiteralError } from '@angular/material/icon';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { IRequestResponseData } from 'src/app/app.types';
import { DoctorService } from '../services/doctor.service';

@Component({
  selector: 'app-doctor-requests',
  templateUrl: './doctor-requests.component.html',
  styleUrls: ['./doctor-requests.component.scss']
})
export class DoctorRequestsComponent implements OnInit {
displayAlert:boolean = false;
@ViewChild(MatPaginator) paginator!:MatPaginator;
@ViewChild('table') myTable!: MatTable<any>;
dataSource!: MatTableDataSource<IRequestResponseData>;
  displayedColumns = [
    'requests',
    'status',
    'reminder'
  ]
  constructor(private doctorService:DoctorService) { }

  ngOnInit(): void {
    this.getRequest();
  }

  getRequest()
  {
    this.doctorService.getData(`status`).subscribe(
      {
        next:(res)=>
        {
          const data = Object(res).data;
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.paginator;
        }
      }
    )
  }

  sendReminder()
  {
    this.doctorService.remindAdmin(`${localStorage.getItem('_id')}`).subscribe((res)=>
    {})
  }
}
