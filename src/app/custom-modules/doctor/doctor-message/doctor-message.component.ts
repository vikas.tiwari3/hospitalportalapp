import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { IMessage, IChangeRequestResult, IChangeRequestResultData } from 'src/app/app.types';
import { DoctorService } from '../services/doctor.service';

@Component({
  selector: 'app-doctor-message',
  templateUrl: './doctor-message.component.html',
  styleUrls: ['./doctor-message.component.scss']
})
export class DoctorMessageComponent implements OnInit {

  displayedColumns = [
    'from',
    'messages',
    'time',
  ]
  resultantData: IChangeRequestResultData[] = [];
  @ViewChild('table') myTable!: MatTable<any>;
  dataSource!: MatTableDataSource<IChangeRequestResult>;

  constructor(private doctorService: DoctorService) { }

  ngOnInit(): void 
  {
    this.getMessages();
  }

  getMessages() 
  {
    this.doctorService.getMessages().subscribe(
      {
        next:(res)=>
        {
          const data = Object(res).data;
          this.dataSource = new MatTableDataSource(data)
        }
      }
    )
  }

}
