import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IChangeNurse, IRequestResponse, IRequestResponseData, IMessageRequestResult, IReminder } from 'src/app/app.types';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  constructor(private httpClient:HttpClient){

  }

  getData(path:string):Observable<IRequestResponse>{
    return this.httpClient.get(`/${path}`) as Observable<IRequestResponse>;
  }

  changeNurse(nurseDataForChange:IChangeNurse):Observable<IChangeNurse>{
    return this.httpClient.post(`/change-request`, nurseDataForChange) as Observable<IChangeNurse>;
  }

  getMessages():Observable<IMessageRequestResult>{
    return this.httpClient.get(`/message`) as Observable<IMessageRequestResult>;
    }

    remindAdmin(id:string):Observable<IReminder>{
      return this.httpClient.post(`/reminder/${id}`,id) as Observable<IReminder>;
     }
}

