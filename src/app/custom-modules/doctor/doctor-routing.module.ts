import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorHomeComponent } from './doctor-home/doctor-home.component';
import { DoctorMessageComponent } from './doctor-message/doctor-message.component';
import { DoctorRequestsComponent } from './doctor-requests/doctor-requests.component';

const routes: Routes = [
    {path: '', component: DoctorHomeComponent},
    {path: 'doctorRequest', component: DoctorRequestsComponent},
    {path: 'doctorMessages', component: DoctorMessageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }