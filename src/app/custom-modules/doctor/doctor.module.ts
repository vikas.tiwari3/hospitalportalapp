import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorHomeComponent } from './doctor-home/doctor-home.component';
import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorMessageComponent } from './doctor-message/doctor-message.component';
import { DoctorRequestsComponent } from './doctor-requests/doctor-requests.component';
import { SharedModule } from '../shared/shared.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    DoctorHomeComponent,
    DoctorMessageComponent,
    DoctorRequestsComponent
  ],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    SharedModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    FormsModule,
   MatTableModule,
   MatIconModule,
   MatSelectModule

  ]
})
export class DoctorModule { }
